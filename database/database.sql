CREATE TABLE Article
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    nom TEXT,
    contenu TEXT,
    tel TEXT,
    horaire TEXT,
    adresse TEXT
);

CREATE TABLE Image
(
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    url TEXT,
    article_id INTEGER,
    FOREIGN KEY(article_id) REFERENCES Article(id)
);
