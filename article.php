<?php 

require 'vendor/autoload.php';

use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Article {
    public $nom;
    public $contenu;
    public $tel;
    public $horaire;
    public $adresse;

    public function __construct($pageUrl, $client) {
        $res = $client->request('GET', $pageUrl);
        $statusCode = $res->getStatusCode();
        $domBody = $res->getBody();
        $crawler = new Crawler((string) $domBody);
        if($statusCode == 200) {
            $this->nom = $crawler->filter('h1')->text();
            $this->contenu = $crawler->filterXPath('//div[@id ="description"]')->text();
            $this->tel = $crawler->filterXPath('//a[@class="phone"]')->text();
            $this->horaire = $crawler->filterXPath('//div[@id = "schedules"]//span')->text();
            $this->adresse = $crawler->filterXPath('//span[@class="address"]/text()[following-sibling::br]')->text();
     
        }
        else {
            echo("Erreur 4xx ou peut etre 5xx, en tout cas ca marche pas");
        }
    }
}


?>