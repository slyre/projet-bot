<?php
require "vendor/autoload.php";
require "article.php";
require "image.php";
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

try {
    $dbh = new PDO('sqlite:database/database.db');
} catch (PDOException $e) {
    echo 'Échec lors de la connexion : ' . $e->getMessage();
}

// toutes les variables nécessaires

$originUrl = 'https://check.fm/restaurant/chambery';
$client = new GuzzleHttp\Client();
$res = $client->request('GET', $originUrl);
$statusCode = $res->getStatusCode();
$domBody = $res->getBody();
$crawler = new Crawler((string) $domBody);
$linksArray = $crawler->filter('#venues a')->extract('href');
$articleArray = [];
$imageArray = [];


// on récupère tous les liens de la page et on les rajoute à l'url de la page

for($i = 0; $i < count($linksArray); $i++) {
    array_push($articleArray, new Article($originUrl.$linksArray[$i], $client));
    array_push($imageArray, new Image($originUrl.$linksArray[$i], $client));
}

// essaie de connecter la BDD, si ça marche pas, msg d'erreur


// double boucle for pour les articles et les images
// on prépare les insert, on execute, on récup le dernier ID de l'article et on boucle ça

for ($i = 0; $i < count($articleArray); $i++) {
    $articleStmt = $dbh->prepare("INSERT INTO Article (nom, contenu, tel, horaire, adresse) VALUES (:nom, :contenu, :tel, :horaire, :adresse)");
    $articleStmt->bindParam(':nom', $articleArray[$i]->nom);
    $articleStmt->bindParam(':contenu', $articleArray[$i]->contenu);
    $articleStmt->bindParam(':tel', $articleArray[$i]->tel);
    $articleStmt->bindParam(':horaire', $articleArray[$i]->horaire);
    $articleStmt->bindParam(':adresse', $articleArray[$i]->adresse);
    $articleStmt->execute();
    $lastArticleId = $dbh->lastInsertId();

    // on prépare l'insert de l'url de l'image et on envoie l'article_id récupéré avec le lastInsertId dans la BDD.

    for ($j = 0; $j < count($imageArray[$i]->url); $j++) {
        $imageStmt = $dbh->prepare("INSERT INTO Image (url, article_id) VALUES (:url, :article_id)");
        $imageStmt->bindParam(':url', $imageArray[$i]->url[$j]);
        $imageStmt->bindParam(':article_id', $lastArticleId);
        $imageStmt->execute();
    }
}
?>
