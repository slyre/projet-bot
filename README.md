# Etapes


1. trouver deux sites 
2. voir quel type d’information
3.  faire un schéma des données (relationnel), 
4. créer la base de données
5. programmation :
a/ Collecter les données
b/ Les enregistrer / stocker (dans la BDD à priori)

# Informations utiles

- faire un script php (qui va chercher des images)
- bdd
- site en rapport avec chambéry (avec des images)
- des images ou des informations (donc liées mais en rapport avec les images en particulier, sur le thème choisit éventuellement), des fichiers json, xml...

- le faire en premier avec une image, des infos qui viennent enrichir l'image (genre date, nom, lieu, description...)

- dire au bot d'aller sur deux sites en particulier


# Etape 1 

Sites choisis : 

1. grandchambery.fr
2. 123savoie.com


# Etape 2 

Type d'informations :

- alt

- l'information autour (dans l'article, qui précise l'image)
- le lieu 
- le nom 
- la description de l'image
- les coordonnées GPS ?