<?php

require 'vendor/autoload.php';
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;

class Image {
    public $url;
    public $article_id;
    public function __construct($pageUrl, $client) {
        
        $res = $client->request('GET', $pageUrl);
        $statusCode = $res->getStatusCode();
        $domBody = $res->getBody();
        $crawler = new Crawler((string) $domBody);
        if($statusCode == 200) {
        
            $this->url = $crawler->filterXPath('//div[@class="ctnr"]/img')->extract(array('src'));
            
        }
        else {
            echo("Erreur 4xx ou peut etre 5xx, en tout cas ca marche pas");
        }
    } 
}
?>